/* This file has been generated by the Hex-Rays decompiler.
   Copyright (c) 2007-2015 Hex-Rays <info@hex-rays.com>

   Detected compiler: GNU C++
*/

#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

int Disp_Switch_Dram_Mode();
int Disp_TVEC_Init();
int Disp_TVEC_Exit();
int Disp_TVEC_Open();
int Disp_TVEC_Close();
int __fastcall Disp_TVEC_DacCfg(int result, int a2);
int BSP_disp_tv_auto_check_enable();
int __fastcall BSP_disp_tv_open(int a1);
int BSP_disp_tv_auto_check_disable();
int __fastcall BSP_disp_tv_close(int a1);
signed int __fastcall BSP_disp_tv_set_mode(int a1, signed int a2);
int __fastcall BSP_disp_tv_get_mode(int a1);
int __fastcall BSP_disp_tv_get_interface(int a1, int a2, int a3, int a4);
int __fastcall BSP_disp_tv_get_dac_status(int a1, int a2);
int __fastcall BSP_disp_tv_set_dac_source(int a1, int a2, int a3);
int __fastcall BSP_disp_tv_get_dac_source(int a1, int a2);
signed int __fastcall BSP_disp_tv_set_src(int a1, int a2);
// int OSAL_printf(const char *, ...); weak
// int TCON1_select_src(void); weak
// int __fastcall tve_clk_off(_DWORD); weak
// int __fastcall TVE_dac_set_source(_DWORD, _DWORD); weak
// int __fastcall tve_clk_on(_DWORD); weak
// int __fastcall TVE_get_dac_status(_DWORD); weak
// int __fastcall disp_de_flicker_enable(_DWORD, _DWORD); weak
// int __fastcall lcdc_clk_off(_DWORD); weak
// int __fastcall image_clk_off(_DWORD); weak
// int __fastcall TCON1_close(_DWORD); weak
// int __fastcall TVE_dac_autocheck_disable(_DWORD); weak
// int __fastcall BSP_disp_enhance_enable(_DWORD, _DWORD); weak
// int __fastcall BSP_disp_set_yuv_output(_DWORD, _DWORD); weak
// int __fastcall TCON1_open(_DWORD); weak
// int __fastcall TVE_set_tv_mode(_DWORD, _DWORD); weak
// int __fastcall TCON1_set_tv_mode(_DWORD, _DWORD); weak
// int __fastcall lcdc_clk_on(_DWORD); weak
// int __fastcall disp_clk_cfg(_DWORD, _DWORD, _DWORD); weak
// int __fastcall Image_open(_DWORD); weak
// int __fastcall image_clk_on(_DWORD); weak
// int __fastcall TVE_dac_autocheck_enable(_DWORD); weak
// int __fastcall TVE_dac_enable(_DWORD); weak
// int __fastcall TVE_close(_DWORD); weak
// int __fastcall TVE_dac_disable(_DWORD); weak
// int TVE_open(void); weak
// int __fastcall tve_clk_exit(_DWORD); weak
// int TVE_exit(void); weak
// int __fastcall TVE_init(_DWORD); weak
// int tve_clk_init(void); weak

//-------------------------------------------------------------------------
// Data declarations

// extern _UNKNOWN gdisp; weak


//----- (00000000) --------------------------------------------------------
int Disp_Switch_Dram_Mode()
{
  return 0;
}

//----- (0000000C) --------------------------------------------------------
int Disp_TVEC_Init()
{
  int v0; // r0@1
  int v1; // r0@1
  int v2; // r0@1
  char *v3; // r1@1
  char *v4; // r1@1
  char *v5; // r1@1

  tve_clk_init();
  v0 = disp_clk_cfg(0, 2, 14);
  v1 = tve_clk_on(v0);
  v2 = TVE_init(v1);
  tve_clk_off(v2);
  v3 = (char *)&gdisp + 88;
  *((_DWORD *)v3 + 105) = *((_DWORD *)&gdisp + 127) & 0xFFBFFFFF;
  v3[407] = 0;
  v4 = (char *)&gdisp + 495;
  v4[1] = 7;
  v4[2] = 7;
  v4[3] = 7;
  *((_BYTE *)&gdisp + 923) = 7;
  v5 = (char *)&gdisp + 923;
  v5[1] = 7;
  v5[2] = 7;
  v5[3] = 7;
  *((_BYTE *)&gdisp + 493) = 14;
  *((_BYTE *)&gdisp + 921) = 14;
  return 0;
}
// AEC: using guessed type int __fastcall tve_clk_off(_DWORD);
// AF4: using guessed type int __fastcall tve_clk_on(_DWORD);
// B2C: using guessed type int __fastcall disp_clk_cfg(_DWORD, _DWORD, _DWORD);
// B54: using guessed type int __fastcall TVE_init(_DWORD);
// B58: using guessed type int tve_clk_init(void);

//----- (00000094) --------------------------------------------------------
int Disp_TVEC_Exit()
{
  int v0; // r0@1

  v0 = TVE_exit();
  tve_clk_exit(v0);
  return 0;
}
// B4C: using guessed type int __fastcall tve_clk_exit(_DWORD);
// B50: using guessed type int TVE_exit(void);

//----- (000000A8) --------------------------------------------------------
int Disp_TVEC_Open()
{
  TVE_open();
  return 0;
}
// B48: using guessed type int TVE_open(void);

//----- (000000C0) --------------------------------------------------------
int Disp_TVEC_Close()
{
  int v0; // r0@1

  TVE_dac_disable(0);
  TVE_dac_disable(1);
  TVE_dac_disable(2);
  v0 = TVE_dac_disable(3);
  TVE_close(v0);
  return 0;
}
// B40: using guessed type int __fastcall TVE_close(_DWORD);
// B44: using guessed type int __fastcall TVE_dac_disable(_DWORD);

//----- (000000F0) --------------------------------------------------------
int __fastcall Disp_TVEC_DacCfg(int result, int a2)
{
  int v2; // r4@1
  unsigned int j; // r6@2
  unsigned int k; // r6@8
  unsigned int l; // r6@16
  unsigned int i; // r6@26

  v2 = result;
  switch ( a2 )
  {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 9:
    case 10:
      for ( i = 0; i < 4; ++i )
      {
        if ( *((_BYTE *)&gdisp + 428 * v2 + i + 495) == 4 )
        {
          TVE_dac_set_source(i, 4);
          result = TVE_dac_enable((unsigned __int8)i);
        }
        else if ( *((_BYTE *)&gdisp + 428 * v2 + i + 495) == 5 )
        {
          TVE_dac_set_source(i, 5);
          result = TVE_dac_enable((unsigned __int8)i);
        }
        else
        {
          result = *((_BYTE *)&gdisp + 428 * v2 + i + 495);
          if ( result == 6 )
          {
            TVE_dac_set_source(i, 6);
            result = TVE_dac_enable((unsigned __int8)i);
          }
        }
      }
      break;
    default:
      return result;
    case 11:
    case 14:
    case 17:
    case 20:
      for ( j = 0; j < 4; ++j )
      {
        result = *((_BYTE *)&gdisp + 428 * v2 + j + 495);
        if ( !*((_BYTE *)&gdisp + 428 * v2 + j + 495) )
        {
          TVE_dac_set_source(j, 0);
          result = TVE_dac_enable((unsigned __int8)j);
        }
      }
      break;
    case 12:
    case 15:
    case 18:
    case 21:
      for ( k = 0; k < 4; ++k )
      {
        if ( *((_BYTE *)&gdisp + 428 * v2 + k + 495) == 1 )
        {
          TVE_dac_set_source(k, 1);
          result = TVE_dac_enable((unsigned __int8)k);
        }
        else
        {
          result = *((_BYTE *)&gdisp + 428 * v2 + k + 495);
          if ( result == 2 )
          {
            TVE_dac_set_source(k, 2);
            result = TVE_dac_enable((unsigned __int8)k);
          }
        }
      }
      break;
    case 13:
    case 16:
    case 19:
    case 22:
      for ( l = 0; l < 4; ++l )
      {
        if ( *((_BYTE *)&gdisp + 428 * v2 + l + 495) )
        {
          if ( *((_BYTE *)&gdisp + 428 * v2 + l + 495) == 1 )
          {
            TVE_dac_set_source(l, 1);
            result = TVE_dac_enable((unsigned __int8)l);
          }
          else
          {
            result = *((_BYTE *)&gdisp + 428 * v2 + l + 495);
            if ( result == 2 )
            {
              TVE_dac_set_source(l, 2);
              result = TVE_dac_enable((unsigned __int8)l);
            }
          }
        }
        else
        {
          TVE_dac_set_source(l, 0);
          result = TVE_dac_enable((unsigned __int8)l);
        }
      }
      break;
  }
  return result;
}
// AF0: using guessed type int __fastcall TVE_dac_set_source(_DWORD, _DWORD);
// B3C: using guessed type int __fastcall TVE_dac_enable(_DWORD);

//----- (0000043C) --------------------------------------------------------
int BSP_disp_tv_auto_check_enable()
{
  TVE_dac_autocheck_enable(0);
  TVE_dac_autocheck_enable(1);
  TVE_dac_autocheck_enable(2);
  TVE_dac_autocheck_enable(3);
  return 0;
}
// B38: using guessed type int __fastcall TVE_dac_autocheck_enable(_DWORD);

//----- (0000046C) --------------------------------------------------------
int __fastcall BSP_disp_tv_open(int a1)
{
  int v1; // r4@1
  int v2; // r5@2
  int v3; // r0@2

  v1 = a1;
  if ( !(*((_DWORD *)&gdisp + 107 * a1 + 22) & 0x20000) )
  {
    v2 = *((_BYTE *)&gdisp + 428 * a1 + 493);
    image_clk_on(a1);
    Image_open(v1);
    v3 = disp_clk_cfg(v1, 2, v2);
    tve_clk_on(v3);
    lcdc_clk_on(v1);
    TCON1_set_tv_mode(v1, v2);
    TVE_set_tv_mode(v1, v2);
    Disp_TVEC_DacCfg(v1, v2);
    TCON1_open(v1);
    Disp_TVEC_Open();
    Disp_Switch_Dram_Mode();
    BSP_disp_set_yuv_output(v1, 1);
    BSP_disp_enhance_enable(v1, 1);
    disp_de_flicker_enable(v1, 1);
    BSP_disp_tv_auto_check_enable();
    *((_DWORD *)&gdisp + 107 * v1 + 22) |= 0x20000u;
    *((_DWORD *)&gdisp + 107 * v1 + 23) |= 0x100u;
    *((_BYTE *)&gdisp + 428 * v1 + 96) = 2;
  }
  return 0;
}
// AF4: using guessed type int __fastcall tve_clk_on(_DWORD);
// B00: using guessed type int __fastcall disp_de_flicker_enable(_DWORD, _DWORD);
// B14: using guessed type int __fastcall BSP_disp_enhance_enable(_DWORD, _DWORD);
// B18: using guessed type int __fastcall BSP_disp_set_yuv_output(_DWORD, _DWORD);
// B1C: using guessed type int __fastcall TCON1_open(_DWORD);
// B20: using guessed type int __fastcall TVE_set_tv_mode(_DWORD, _DWORD);
// B24: using guessed type int __fastcall TCON1_set_tv_mode(_DWORD, _DWORD);
// B28: using guessed type int __fastcall lcdc_clk_on(_DWORD);
// B2C: using guessed type int __fastcall disp_clk_cfg(_DWORD, _DWORD, _DWORD);
// B30: using guessed type int __fastcall Image_open(_DWORD);
// B34: using guessed type int __fastcall image_clk_on(_DWORD);

//----- (000005A0) --------------------------------------------------------
int BSP_disp_tv_auto_check_disable()
{
  TVE_dac_autocheck_disable(0);
  TVE_dac_autocheck_disable(1);
  TVE_dac_autocheck_disable(2);
  TVE_dac_autocheck_disable(3);
  return 0;
}
// B10: using guessed type int __fastcall TVE_dac_autocheck_disable(_DWORD);

//----- (000005D0) --------------------------------------------------------
int __fastcall BSP_disp_tv_close(int a1)
{
  int v1; // r4@1
  int v2; // r0@2

  v1 = a1;
  if ( *((_DWORD *)&gdisp + 107 * a1 + 22) & 0x20000 )
  {
    TCON1_close(a1);
    v2 = Disp_TVEC_Close();
    tve_clk_off(v2);
    image_clk_off(v1);
    lcdc_clk_off(v1);
    disp_de_flicker_enable(v1, 0);
    BSP_disp_tv_auto_check_disable();
    *((_DWORD *)&gdisp + 107 * v1 + 22) &= 0xFFFDFFFF;
    *((_DWORD *)&gdisp + 107 * v1 + 23) &= 0xFFFFFEFF;
    *((_BYTE *)&gdisp + 428 * v1 + 96) = 0;
    *((_DWORD *)&gdisp + 107 * v1 + 127) &= 0xFFBFFFFF;
  }
  return 0;
}
// AEC: using guessed type int __fastcall tve_clk_off(_DWORD);
// B00: using guessed type int __fastcall disp_de_flicker_enable(_DWORD, _DWORD);
// B04: using guessed type int __fastcall lcdc_clk_off(_DWORD);
// B08: using guessed type int __fastcall image_clk_off(_DWORD);
// B0C: using guessed type int __fastcall TCON1_close(_DWORD);

//----- (000006B4) --------------------------------------------------------
signed int __fastcall BSP_disp_tv_set_mode(int a1, signed int a2)
{
  signed int result; // r0@2

  if ( a2 <= 22 )
  {
    *((_BYTE *)&gdisp + 428 * a1 + 493) = a2;
    *((_BYTE *)&gdisp + 428 * a1 + 96) = 2;
    result = 0;
  }
  else
  {
    OSAL_printf("[DISP WRN]L%d(%s):", 238, "disp_tv.c");
    OSAL_printf("unsupported tv mode in BSP_disp_tv_set_mode\n");
    result = -1;
  }
  return result;
}
// AE4: using guessed type int OSAL_printf(const char *, ...);

//----- (0000071C) --------------------------------------------------------
int __fastcall BSP_disp_tv_get_mode(int a1)
{
  return *((_BYTE *)&gdisp + 428 * a1 + 493);
}

//----- (00000738) --------------------------------------------------------
int __fastcall BSP_disp_tv_get_interface(int a1, int a2, int a3, int a4)
{
  int v4; // r4@1
  int v5; // r6@1
  int v6; // r0@1
  signed int i; // r5@3
  signed int j; // r5@11
  int v9; // r0@23
  int v11; // [sp+0h] [bp-18h]@1

  v11 = a4;
  v4 = a1;
  v5 = 0;
  v6 = *((_DWORD *)&gdisp + 107 * a1 + 22);
  if ( !(v6 & 0x20000) )
    tve_clk_on(v6);
  for ( i = 0; i < 4; ++i )
    *((_BYTE *)&v11 + i) = TVE_get_dac_status(i);
  if ( (signed int)(unsigned __int8)v11 <= 1
    && (signed int)BYTE1(v11) <= 1
    && (signed int)BYTE2(v11) <= 1
    && (signed int)BYTE3(v11) <= 1 )
  {
    for ( j = 0; j < 4; ++j )
    {
      if ( *((_BYTE *)&gdisp + 428 * v4 + j + 495) || *((_BYTE *)&v11 + j) != 1 )
      {
        if ( *((_BYTE *)&gdisp + 428 * v4 + j + 495) != 4 || *((_BYTE *)&v11 + j) != 1 )
        {
          if ( *((_BYTE *)&gdisp + 428 * v4 + j + 495) == 1 && *((_BYTE *)&v11 + j) == 1 )
            v5 |= 4u;
        }
        else
        {
          v5 |= 2u;
        }
      }
      else
      {
        v5 |= 1u;
      }
    }
  }
  else
  {
    OSAL_printf("[DISP WRN]L%d(%s):", 272, "disp_tv.c");
    OSAL_printf("shor to ground\n");
  }
  v9 = *((_DWORD *)&gdisp + 107 * v4 + 22);
  if ( !(v9 & 0x20000) )
    tve_clk_off(v9);
  return v5;
}
// AE4: using guessed type int OSAL_printf(const char *, ...);
// AEC: using guessed type int __fastcall tve_clk_off(_DWORD);
// AF4: using guessed type int __fastcall tve_clk_on(_DWORD);
// AF8: using guessed type int __fastcall TVE_get_dac_status(_DWORD);

//----- (00000918) --------------------------------------------------------
int __fastcall BSP_disp_tv_get_dac_status(int a1, int a2)
{
  int v2; // r4@1
  int v3; // r5@1
  int v4; // r0@1
  int v5; // r6@3
  int v6; // r0@3

  v2 = a1;
  v3 = a2;
  v4 = *((_DWORD *)&gdisp + 107 * a1 + 22);
  if ( !(v4 & 0x20000) )
    tve_clk_on(v4);
  v5 = TVE_get_dac_status(v3);
  v6 = *((_DWORD *)&gdisp + 107 * v2 + 22);
  if ( !(v6 & 0x20000) )
    tve_clk_off(v6);
  return v5;
}
// AEC: using guessed type int __fastcall tve_clk_off(_DWORD);
// AF4: using guessed type int __fastcall tve_clk_on(_DWORD);
// AF8: using guessed type int __fastcall TVE_get_dac_status(_DWORD);

//----- (00000970) --------------------------------------------------------
int __fastcall BSP_disp_tv_set_dac_source(int a1, int a2, int a3)
{
  int v3; // r4@1
  int v4; // r5@1
  int v5; // r6@1
  int v6; // r0@1
  int v7; // r7@3
  int v8; // r0@3

  v3 = a1;
  v4 = a2;
  v5 = a3;
  v6 = *((_DWORD *)&gdisp + 107 * a1 + 22);
  if ( !(v6 & 0x20000) )
    tve_clk_on(v6);
  v7 = TVE_dac_set_source(v4, v5);
  v8 = *((_DWORD *)&gdisp + 107 * v3 + 22);
  if ( !(v8 & 0x20000) )
    tve_clk_off(v8);
  *((_BYTE *)&gdisp + 428 * v3 + v4 + 495) = v5;
  return v7;
}
// AEC: using guessed type int __fastcall tve_clk_off(_DWORD);
// AF0: using guessed type int __fastcall TVE_dac_set_source(_DWORD, _DWORD);
// AF4: using guessed type int __fastcall tve_clk_on(_DWORD);

//----- (000009EC) --------------------------------------------------------
int __fastcall BSP_disp_tv_get_dac_source(int a1, int a2)
{
  return *((_BYTE *)&gdisp + 428 * a1 + a2 + 495);
}

//----- (00000A10) --------------------------------------------------------
signed int __fastcall BSP_disp_tv_set_src(int a1, int a2)
{
  int v2; // r4@1

  v2 = a2;
  if ( a2 )
  {
    if ( a2 == 1 )
    {
      TCON1_select_src();
    }
    else
    {
      if ( a2 != 5 )
      {
        OSAL_printf("[DISP WRN]L%d(%s):", 386, "disp_tv.c");
        OSAL_printf("not supported lcdc src:%d in BSP_disp_tv_set_src\n", v2);
        return -5;
      }
      TCON1_select_src();
    }
  }
  else
  {
    TCON1_select_src();
  }
  return 0;
}
// AE4: using guessed type int OSAL_printf(const char *, ...);
// AE8: using guessed type int TCON1_select_src(void);

// ALL OK, 17 function(s) have been successfully decompiled
