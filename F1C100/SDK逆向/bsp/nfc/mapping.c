/* This file has been generated by the Hex-Rays decompiler.
   Copyright (c) 2007-2015 Hex-Rays <info@hex-rays.com>

   Detected compiler: GNU C++
*/

#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

_DWORD *__fastcall ebase_init_critical(_DWORD *result);
unsigned int ebase_enter_critical();
unsigned int __fastcall ebase_exit_critical(unsigned int result);
int __fastcall dump(int a1, unsigned int a2, unsigned int a3, unsigned int a4);
int __fastcall GetTblCheckSum(int a1, unsigned int a2);
signed int PMM_InitMapTblCache();
unsigned int CalPageTblAccessCount();
int PMM_ExitMapTblCache();
signed int __fastcall page_map_tbl_cache_hit(int a1);
unsigned int find_page_tbl_post_location();
signed int __fastcall write_back_page_map_tbl(int a1);
int __fastcall write_dirty_flag(int a1);
int BMM_SetDirtyFlag();
signed int __fastcall rebuild_page_map_tbl(int a1);
signed int __fastcall read_page_map_tbl(int a1);
signed int __fastcall page_map_tbl_cache_post(int a1);
signed int __fastcall PMM_SwitchMapTbl(int a1);
signed int BMM_InitMapTblCache();
unsigned int CalBlkTblAccessCount();
int BMM_ExitMapTblCache();
signed int __fastcall blk_map_tbl_cache_hit(int a1);
unsigned int find_blk_tbl_post_location();
signed int __fastcall write_back_all_page_map_tbl(int a1);
signed int __fastcall write_back_block_map_tbl(int a1);
signed int __fastcall read_block_map_tbl(int a1, int a2, int a3, int a4);
signed int __fastcall blk_map_tbl_cache_post(unsigned __int8 a1);
signed int __fastcall BMM_SwitchMapTbl(int a1);
signed int BMM_WriteBackAllMapTbl();
// int OSAL_printf(const char *, ...); weak
// int __fastcall _aeabi_memcpy(_DWORD, _DWORD, _DWORD); weak
// int __fastcall LML_VirtualPageRead(_DWORD); weak
// int __fastcall LML_CalculatePhyOpPar(_DWORD, _DWORD, _DWORD, _DWORD); weak
// int __fastcall LML_BadBlkManage(_DWORD, _DWORD, _DWORD, _DWORD); weak
// int __fastcall PHY_SynchBank(_DWORD, _DWORD); weak
// int __fastcall LML_VirtualPageWrite(_DWORD); weak
// int __fastcall _aeabi_memset(_DWORD, _DWORD, _DWORD); weak
// int __fastcall LML_VirtualBlkErase(_DWORD, _DWORD); weak
// int __fastcall OSAL_PhyFree(_DWORD, _DWORD); weak
// int __fastcall _aeabi_memclr(_DWORD, _DWORD); weak
// int __fastcall OSAL_PhyAlloc(_DWORD); weak
// int __fastcall LML_MergeLogBlk(_DWORD, _DWORD); weak

//-------------------------------------------------------------------------
// Data declarations

_UNKNOWN BlkMapTblCachePool; // weak
_UNKNOWN PageMapTblCachePool; // weak
// extern _UNKNOWN NandStorageInfo; weak
// extern _UNKNOWN NandDriverInfo; weak


//----- (00000000) --------------------------------------------------------
_DWORD *__fastcall ebase_init_critical(_DWORD *result)
{
  *result = 0;
  return result;
}

//----- (0000000C) --------------------------------------------------------
unsigned int ebase_enter_critical()
{
  unsigned int result; // r0@1

  result = __get_CPSR();
  __set_CPSR(result | 0x80);
  return result;
}

//----- (00000024) --------------------------------------------------------
unsigned int __fastcall ebase_exit_critical(unsigned int result)
{
  __set_CPSR(result);
  return result;
}

//----- (00000034) --------------------------------------------------------
int __fastcall dump(int a1, unsigned int a2, unsigned int a3, unsigned int a4)
{
  int v4; // r4@1
  unsigned int v5; // r5@1
  unsigned int v6; // r6@1
  unsigned int v7; // r9@1
  int result; // r0@1
  unsigned int i; // r8@1

  v4 = a1;
  v5 = a3;
  v6 = a4;
  v7 = a2 / a3;
  result = ((int (__fastcall *)(const char *, unsigned int))OSAL_printf)(
             "/********************************************/\n",
             a2 % a3);
  for ( i = 0; i < v7; ++i )
  {
    if ( v5 == 1 )
    {
      OSAL_printf("%x  ", *(_BYTE *)(v4 + i));
    }
    else if ( v5 == 2 )
    {
      OSAL_printf("%x  ", *(_WORD *)(v4 + 2 * i));
    }
    else
    {
      if ( v5 != 4 )
        return result;
      OSAL_printf("%x  ", *(_DWORD *)(v4 + 4 * i));
    }
    result = v6 - 1;
    if ( i % v6 == v6 - 1 )
      result = OSAL_printf("\n");
  }
  return result;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);

//----- (000000EC) --------------------------------------------------------
int __fastcall GetTblCheckSum(int a1, unsigned int a2)
{
  int v2; // r2@1
  int result; // r0@1
  unsigned int i; // r3@1

  v2 = a1;
  result = 0;
  for ( i = 0; i < a2; ++i )
    result += *(_DWORD *)(v2 + 4 * i);
  return result;
}

//----- (00000114) --------------------------------------------------------
signed int PMM_InitMapTblCache()
{
  unsigned int i; // r4@1

  *((_DWORD *)&NandDriverInfo + 3) = &PageMapTblCachePool;
  for ( i = 0; i < 4; ++i )
  {
    *(_WORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 2) = 0;
    *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 8) = 0;
    *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 1) = -1;
    *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i) = -1;
    *(_DWORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 4) = OSAL_PhyAlloc(2 * *(_WORD *)(*((_DWORD *)&NandDriverInfo
                                                                                                  + 4)
                                                                                                + 2));
    if ( !*(_DWORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 4) )
      return -18;
  }
  return 0;
}
// 1FF4: using guessed type int __fastcall OSAL_PhyAlloc(_DWORD);

//----- (000001F8) --------------------------------------------------------
unsigned int CalPageTblAccessCount()
{
  unsigned int result; // r0@1

  for ( result = 0; result < 4; ++result )
    ++*(_WORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * result + 2);
  *(_WORD *)(**((_DWORD **)&NandDriverInfo + 3) + 2) = 0;
  return result;
}

//----- (000002A0) --------------------------------------------------------
int PMM_ExitMapTblCache()
{
  unsigned int i; // r4@1

  for ( i = 0; i < 4; ++i )
    OSAL_PhyFree(
      *(_DWORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 4),
      2 * *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2));
  return 0;
}
// 1FEC: using guessed type int __fastcall OSAL_PhyFree(_DWORD, _DWORD);

//----- (000002EC) --------------------------------------------------------
signed int __fastcall page_map_tbl_cache_hit(int a1)
{
  unsigned int i; // r2@1

  for ( i = 0; i < 4; ++i )
  {
    if ( *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i) == ***((_BYTE ***)&NandDriverInfo + 2)
      && *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 1) == a1 )
    {
      **((_DWORD **)&NandDriverInfo + 3) = *((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i;
      return 0;
    }
  }
  return -1;
}

//----- (00000380) --------------------------------------------------------
unsigned int find_page_tbl_post_location()
{
  unsigned int v0; // r2@1
  unsigned int i; // r1@1
  signed int v3; // r3@7
  unsigned int j; // r1@7
  unsigned int k; // r1@12

  v0 = 0;
  for ( i = 0; i < 4; ++i )
  {
    if ( *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i) == 255 )
      return i;
  }
  v3 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 3) + 6);
  for ( j = 1; j < 4; ++j )
  {
    if ( *(_WORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * j + 2) > v3 )
    {
      v0 = j;
      v3 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * j + 2);
    }
  }
  for ( k = 0; k < 4; ++k )
    *(_WORD *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * k + 2) = 0;
  return v0;
}

//----- (00000454) --------------------------------------------------------
signed int __fastcall write_back_page_map_tbl(int a1)
{
  int v1; // r4@1
  unsigned int v2; // r6@1
  int v3; // r7@1
  signed int result; // r0@3
  unsigned int i; // r5@9
  int v19; // r0@17
  unsigned __int16 v20; // [sp+4h] [bp-3Ch]@15
  __int16 v21; // [sp+6h] [bp-3Ah]@17
  __int16 v22; // [sp+8h] [bp-38h]@15
  unsigned __int8 v23; // [sp+Ch] [bp-34h]@14
  int v24; // [sp+10h] [bp-30h]@14
  int v25; // [sp+14h] [bp-2Ch]@14
  char *v26; // [sp+18h] [bp-28h]@14
  char v27; // [sp+1Ch] [bp-24h]@8
  char v28; // [sp+22h] [bp-1Eh]@8

  v1 = a1;
  v2 = (*(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * a1 + 2) + 1) & 0xFFFEFFFF;
  v3 = *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * a1 + 4);
  if ( *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) != v2 )
    goto LABEL_8;
  if ( LML_MergeLogBlk(1, *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * a1)) )
  {
    OSAL_printf("write back page tbl : merge err\n");
    result = -1;
  }
  else if ( ***((_BYTE ***)&NandDriverInfo + 3) == 255 )
  {
    result = 0;
  }
  else
  {
    v2 = (*(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * v1 + 2) + 1) & 0xFFFEFFFF;
    v3 = *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * v1 + 4);
LABEL_8:
    while ( 1 )
    {
      _aeabi_memset(&v27, 16, 255);
      v28 = -86;
      _R2 = *((_BYTE *)&NandStorageInfo + 10);
      _R3 = *((_BYTE *)&NandStorageInfo + 9);
      __asm { SMULBB          R2, R2, R3 }
      _aeabi_memset(**((_DWORD **)&NandDriverInfo + 5), _R2 << 9, 255);
      if ( (signed int)*(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) < 512 )
      {
        _aeabi_memcpy(
          **((_DWORD **)&NandDriverInfo + 5),
          *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 3) + 4),
          2 * *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2));
        *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 5) + 2044) = GetTblCheckSum(
                                                                   **((_DWORD **)&NandDriverInfo + 5),
                                                                   2
                                                                 * (unsigned int)*(_WORD *)(*((_DWORD *)&NandDriverInfo
                                                                                            + 4)
                                                                                          + 2) >> 2);
      }
      else
      {
        for ( i = 0; *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) > i; ++i )
          *(_WORD *)(**((_DWORD **)&NandDriverInfo + 5) + 2 * i) = *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo
                                                                                           + 3)
                                                                                        + 4)
                                                                            + 2 * i);
        *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 5) + 2044) = GetTblCheckSum(
                                                                   **((_DWORD **)&NandDriverInfo + 5),
                                                                   2
                                                                 * (unsigned int)*(_WORD *)(*((_DWORD *)&NandDriverInfo
                                                                                            + 4)
                                                                                          + 2) >> 2);
      }
      v25 = **((_DWORD **)&NandDriverInfo + 5);
      v26 = &v27;
      _R0 = *((_BYTE *)&NandStorageInfo + 10);
      _R1 = *((_BYTE *)&NandStorageInfo + 9);
      __asm { SMULBB          R0, R0, R1 }
      _R1 = *((_BYTE *)&NandStorageInfo + 10);
      _R2 = *((_BYTE *)&NandStorageInfo + 9);
      __asm { SMULBB          R1, R1, R2 }
      v24 = (1 << (_R0 - 1)) | ((1 << (_R1 - 1)) - 1);
      LML_CalculatePhyOpPar(&v23, ***((_BYTE ***)&NandDriverInfo + 2), v3, v2);
      LML_VirtualPageWrite(&v23);
      if ( !PHY_SynchBank(v23, 0) )
        break;
      v22 = v3;
      if ( LML_BadBlkManage(&v22, ***((_BYTE ***)&NandDriverInfo + 2), v2, &v20) )
      {
        OSAL_printf("write page map table : bad block mange err after write\n");
        return -1;
      }
      v3 = v20;
      v19 = *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * v1;
      *(_WORD *)(v19 + 4) = v20;
      *(_WORD *)(v19 + 6) = v21;
    }
    *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * v1 + 2) = v2;
    ***((_BYTE ***)&NandDriverInfo + 3) = -1;
    *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 1) = -1;
    result = 0;
  }
  return result;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);
// 1FC0: using guessed type int __fastcall _aeabi_memcpy(_DWORD, _DWORD, _DWORD);
// 1FCC: using guessed type int __fastcall LML_CalculatePhyOpPar(_DWORD, _DWORD, _DWORD, _DWORD);
// 1FD0: using guessed type int __fastcall LML_BadBlkManage(_DWORD, _DWORD, _DWORD, _DWORD);
// 1FD4: using guessed type int __fastcall PHY_SynchBank(_DWORD, _DWORD);
// 1FD8: using guessed type int __fastcall LML_VirtualPageWrite(_DWORD);
// 1FDC: using guessed type int __fastcall _aeabi_memset(_DWORD, _DWORD, _DWORD);
// 1FF8: using guessed type int __fastcall LML_MergeLogBlk(_DWORD, _DWORD);

//----- (0000083C) --------------------------------------------------------
int __fastcall write_dirty_flag(int a1)
{
  int v1; // r4@1
  int v2; // r6@1
  int v3; // r5@1
  int v5; // [sp+0h] [bp-30h]@1
  char v6; // [sp+6h] [bp-2Ah]@1
  unsigned __int8 v7; // [sp+10h] [bp-20h]@1
  int v8; // [sp+18h] [bp-18h]@1
  int *v9; // [sp+1Ch] [bp-14h]@1

  v1 = a1;
  v2 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * a1);
  v3 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * a1 + 2) + 3;
  _aeabi_memset(&v5, 16, 255);
  v6 = 85;
  _aeabi_memset(**((_DWORD **)&NandDriverInfo + 5), 512, 85);
  v8 = **((_DWORD **)&NandDriverInfo + 5);
  v9 = &v5;
  LML_CalculatePhyOpPar(&v7, v1, v2, v3);
  LML_VirtualPageWrite(&v7);
  PHY_SynchBank(v7, 0);
  return 0;
}
// 1FCC: using guessed type int __fastcall LML_CalculatePhyOpPar(_DWORD, _DWORD, _DWORD, _DWORD);
// 1FD4: using guessed type int __fastcall PHY_SynchBank(_DWORD, _DWORD);
// 1FD8: using guessed type int __fastcall LML_VirtualPageWrite(_DWORD);
// 1FDC: using guessed type int __fastcall _aeabi_memset(_DWORD, _DWORD, _DWORD);

//----- (000008E4) --------------------------------------------------------
int BMM_SetDirtyFlag()
{
  if ( !*(_BYTE *)(**((_DWORD **)&NandDriverInfo + 2) + 1) )
  {
    write_dirty_flag(***((_BYTE ***)&NandDriverInfo + 2));
    *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 2) + 1) = 1;
  }
  return 0;
}

//----- (00000930) --------------------------------------------------------
signed int __fastcall rebuild_page_map_tbl(int a1)
{
  int v1; // r4@1
  int v2; // r8@1
  signed int i; // r5@1
  int v5; // [sp+0h] [bp-38h]@2
  int v6; // [sp+4h] [bp-34h]@1
  int v7; // [sp+8h] [bp-30h]@1
  char *v8; // [sp+Ch] [bp-2Ch]@1
  char v9; // [sp+10h] [bp-28h]@1
  unsigned __int16 v10; // [sp+14h] [bp-24h]@5

  v1 = a1;
  _aeabi_memset(
    *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 3) + 4),
    2 * *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2),
    255);
  v2 = *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * v1 + 4);
  v7 = **((_DWORD **)&NandDriverInfo + 5);
  v8 = &v9;
  v6 = 3;
  for ( i = 0; *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) > i; i = (i + 1) & 0xFFFEFFFF )
  {
    LML_CalculatePhyOpPar(&v5, ***((_BYTE ***)&NandDriverInfo + 2), v2, i);
    if ( LML_VirtualPageRead(&v5) < 0 )
    {
      OSAL_printf(
        "rebuild logic block %x page map table : read err\n",
        *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * v1));
      return -1;
    }
    if ( v10 != 0xFFFF && *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) > (signed int)v10 )
      *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 3) + 4) + 2 * v10) = i;
  }
  *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 8) = 1;
  BMM_SetDirtyFlag();
  return 0;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);
// 1FC8: using guessed type int __fastcall LML_VirtualPageRead(_DWORD);
// 1FCC: using guessed type int __fastcall LML_CalculatePhyOpPar(_DWORD, _DWORD, _DWORD, _DWORD);
// 1FDC: using guessed type int __fastcall _aeabi_memset(_DWORD, _DWORD, _DWORD);

//----- (00000A7C) --------------------------------------------------------
signed int __fastcall read_page_map_tbl(int a1)
{
  int v1; // r4@1
  int v2; // r7@1
  int v3; // r10@1
  int v5; // r6@4
  int v6; // r5@4
  unsigned int i; // r0@11
  int v8; // [sp+0h] [bp-40h]@4
  int v9; // [sp+4h] [bp-3Ch]@4
  int v10; // [sp+8h] [bp-38h]@4
  char *v11; // [sp+Ch] [bp-34h]@4
  char v12; // [sp+10h] [bp-30h]@4
  unsigned __int16 v13; // [sp+14h] [bp-2Ch]@6
  unsigned __int8 v14; // [sp+16h] [bp-2Ah]@5

  v1 = a1;
  v2 = *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * a1 + 2);
  v3 = *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8) + 8 * a1 + 4);
  if ( v2 == 0xFFFF )
  {
    _aeabi_memset(
      *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 3) + 4),
      2 * *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2),
      255);
    return 0;
  }
  v10 = **((_DWORD **)&NandDriverInfo + 5);
  v11 = &v12;
  v9 = 15;
  LML_CalculatePhyOpPar(&v8, ***((_BYTE ***)&NandDriverInfo + 2), v3, v2);
  v5 = LML_VirtualPageRead(&v8);
  v6 = GetTblCheckSum(
         **((_DWORD **)&NandDriverInfo + 5),
         2 * (unsigned int)*(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) >> 2);
  if ( v5 >= 0 && v14 == 170 && v13 == 0xFFFF && *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 5) + 2044) == v6 )
  {
    if ( (signed int)*(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) < 512 )
    {
      _aeabi_memcpy(
        *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 3) + 4),
        **((_DWORD **)&NandDriverInfo + 5),
        2 * *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2));
    }
    else
    {
      for ( i = 0; *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) > i; ++i )
        *(_WORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 3) + 4) + 2 * i) = *(_WORD *)(**((_DWORD **)&NandDriverInfo
                                                                                              + 5)
                                                                                           + 2 * i);
    }
  }
  else if ( rebuild_page_map_tbl(v1) )
  {
    OSAL_printf("rebuild page map table err\n");
    return -1;
  }
  return 0;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);
// 1FC0: using guessed type int __fastcall _aeabi_memcpy(_DWORD, _DWORD, _DWORD);
// 1FC8: using guessed type int __fastcall LML_VirtualPageRead(_DWORD);
// 1FCC: using guessed type int __fastcall LML_CalculatePhyOpPar(_DWORD, _DWORD, _DWORD, _DWORD);
// 1FDC: using guessed type int __fastcall _aeabi_memset(_DWORD, _DWORD, _DWORD);

//----- (00000CF4) --------------------------------------------------------
signed int __fastcall page_map_tbl_cache_post(int a1)
{
  int v1; // r4@1
  _BYTE *v2; // r6@1
  int v3; // r7@1
  int i; // r5@4
  signed int result; // r0@10

  v1 = a1;
  v2 = (_BYTE *)**((_DWORD **)&NandDriverInfo + 2);
  v3 = (unsigned __int8)find_page_tbl_post_location();
  **((_DWORD **)&NandDriverInfo + 3) = *((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * v3;
  if ( *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 8) && ***((_BYTE ***)&NandDriverInfo + 3) != 255 )
  {
    if ( ***((_BYTE ***)&NandDriverInfo + 3) != *v2 )
    {
      for ( i = 0; i < 4; i = (i + 1) & 0xFF )
      {
        if ( *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i) == ***((_BYTE ***)&NandDriverInfo + 3) )
        {
          **((_DWORD **)&NandDriverInfo + 2) = *((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i;
          break;
        }
      }
      if ( i == 4 )
      {
        OSAL_printf(
          "_page_map_tbl_cache_post : position %d ,page map zone %d,blk map zone %d\n",
          v3,
          ***((_BYTE ***)&NandDriverInfo + 3),
          ***((_BYTE ***)&NandDriverInfo + 2));
        return -1;
      }
    }
    BMM_SetDirtyFlag();
    if ( write_back_page_map_tbl(*(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 1)) )
    {
      OSAL_printf("write back page tbl err\n");
      return -1;
    }
    **((_DWORD **)&NandDriverInfo + 2) = v2;
  }
  *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 8) = 0;
  if ( read_page_map_tbl(v1) )
  {
    OSAL_printf("read page map tbl err\n");
    result = -1;
  }
  else
  {
    ***((_BYTE ***)&NandDriverInfo + 3) = ***((_BYTE ***)&NandDriverInfo + 2);
    *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 1) = v1;
    result = 0;
  }
  return result;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);

//----- (00000ECC) --------------------------------------------------------
signed int __fastcall PMM_SwitchMapTbl(int a1)
{
  int v1; // r4@1
  signed int v2; // r5@1

  v1 = a1;
  v2 = 0;
  if ( page_map_tbl_cache_hit(a1) )
    v2 = page_map_tbl_cache_post(v1);
  CalPageTblAccessCount();
  return v2;
}

//----- (00000F00) --------------------------------------------------------
signed int BMM_InitMapTblCache()
{
  unsigned int i; // r4@1

  *((_DWORD *)&NandDriverInfo + 2) = &BlkMapTblCachePool;
  *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 100) = 0;
  *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 102) = 0;
  for ( i = 0; i < 4; ++i )
  {
    *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i) = -1;
    *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 1) = 0;
    *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 2) = 0;
    *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 16) = 255;
    *(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 4) = OSAL_PhyAlloc(4096);
    if ( !*(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 4) )
    {
      OSAL_printf("BMM_InitMapTblCache : allocate memory err\n");
      return -11;
    }
    *(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 12) = *(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2)
                                                                                + 4
                                                                                + 20 * i
                                                                                + 4)
                                                                    + 4 * **((_WORD **)&NandDriverInfo + 4);
    *(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 8) = OSAL_PhyAlloc(64);
    if ( !*(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 8) )
    {
      OSAL_printf("BMM_InitMapTblCache : allocate memory err\n");
      OSAL_PhyFree(*(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 4), 4096);
      return -11;
    }
  }
  _aeabi_memclr(*((_DWORD *)&NandDriverInfo + 2) + 84, 8);
  return 0;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);
// 1FEC: using guessed type int __fastcall OSAL_PhyFree(_DWORD, _DWORD);
// 1FF0: using guessed type int __fastcall _aeabi_memclr(_DWORD, _DWORD);
// 1FF4: using guessed type int __fastcall OSAL_PhyAlloc(_DWORD);

//----- (00001128) --------------------------------------------------------
unsigned int CalBlkTblAccessCount()
{
  unsigned int result; // r0@1

  for ( result = 0; result < 4; ++result )
    ++*(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * result + 2);
  *(_WORD *)(**((_DWORD **)&NandDriverInfo + 2) + 2) = 0;
  return result;
}

//----- (0000118C) --------------------------------------------------------
int BMM_ExitMapTblCache()
{
  unsigned int i; // r4@1

  for ( i = 0; i < 4; ++i )
  {
    OSAL_PhyFree(*(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 4), 4096);
    OSAL_PhyFree(*(_DWORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 8), 64);
  }
  return 0;
}
// 1FEC: using guessed type int __fastcall OSAL_PhyFree(_DWORD, _DWORD);

//----- (000011EC) --------------------------------------------------------
signed int __fastcall blk_map_tbl_cache_hit(int a1)
{
  unsigned int i; // r2@1

  for ( i = 0; i < 4; ++i )
  {
    if ( *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i) == a1 )
    {
      **((_DWORD **)&NandDriverInfo + 2) = *((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i;
      return 0;
    }
  }
  return -1;
}

//----- (00001250) --------------------------------------------------------
unsigned int find_blk_tbl_post_location()
{
  unsigned int i; // r1@1
  int v2; // r3@7
  signed int v3; // r2@7
  unsigned int j; // r1@7
  unsigned int k; // r1@12

  for ( i = 0; i < 4; ++i )
  {
    if ( *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i) == 255 )
      return i;
  }
  v2 = 0;
  v3 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 6);
  for ( j = 1; j < 4; ++j )
  {
    if ( *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * j + 2) > v3 )
    {
      v2 = (unsigned __int8)j;
      v3 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * j + 2);
    }
  }
  for ( k = 0; k < 4; ++k )
    *(_WORD *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * k + 2) = 0;
  return v2;
}

//----- (00001324) --------------------------------------------------------
signed int __fastcall write_back_all_page_map_tbl(int a1)
{
  int v1; // r4@1
  unsigned int i; // r5@1

  v1 = a1;
  for ( i = 0; i < 4; ++i )
  {
    if ( *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i) == v1
      && *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i + 8) == 1 )
    {
      **((_DWORD **)&NandDriverInfo + 3) = *((_DWORD *)&NandDriverInfo + 3) + 4 + 12 * i;
      if ( write_back_page_map_tbl(*(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 1)) )
      {
        OSAL_printf("write back all page tbl : write page map table err \n");
        return -1;
      }
      *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 3) + 8) = 0;
    }
  }
  return 0;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);

//----- (000013E4) --------------------------------------------------------
signed int __fastcall write_back_block_map_tbl(int a1)
{
  signed int v1; // r4@1
  int v3; // r6@4
  int v4; // r5@4
  int v5; // r5@10
  int v6; // r3@10
  int v14; // r0@12
  int v21; // r5@17
  int v22; // r5@21
  unsigned __int16 v26; // [sp+0h] [bp-38h]@6
  __int16 v27; // [sp+4h] [bp-34h]@6
  unsigned __int8 v28; // [sp+8h] [bp-30h]@13
  int v29; // [sp+Ch] [bp-2Ch]@13
  int v30; // [sp+10h] [bp-28h]@13
  char *v31; // [sp+14h] [bp-24h]@13
  char v32; // [sp+18h] [bp-20h]@11
  char v33; // [sp+19h] [bp-1Fh]@12
  char v34; // [sp+1Ah] [bp-1Eh]@12
  char v35; // [sp+1Eh] [bp-1Ah]@13

  v1 = a1;
  if ( write_back_all_page_map_tbl(a1) )
  {
    OSAL_printf("write back all page map tbl err\n");
    return -1;
  }
  v3 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * v1);
  v4 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * v1 + 2);
  if ( *(_WORD *)(*((_DWORD *)&NandDriverInfo + 4) + 2) - 4 <= v4 )
  {
    if ( LML_VirtualBlkErase(v1, *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * v1)) )
    {
      v27 = v3;
      if ( LML_BadBlkManage(&v27, ***((_BYTE ***)&NandDriverInfo + 2), 0, &v26) )
      {
        OSAL_printf("write back block tbl : bad block manage err erase data block\n");
        return -1;
      }
      v3 = v26;
    }
    v4 = -4;
  }
  v5 = v4 + 4;
  v6 = 1023 - **((_WORD **)&NandDriverInfo + 4);
  *(_DWORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4) + 4092) = GetTblCheckSum(
                                                                              *(_DWORD *)(**((_DWORD **)&NandDriverInfo
                                                                                           + 2)
                                                                                        + 4),
                                                                              0x3FFu);
  _R2 = *((_BYTE *)&NandStorageInfo + 10);
  _R3 = *((_BYTE *)&NandStorageInfo + 9);
  __asm { SMULBB          R2, R2, R3 }
  _aeabi_memset(**((_DWORD **)&NandDriverInfo + 5), _R2 << 9, 255);
  while ( 1 )
  {
    while ( 1 )
    {
      while ( 1 )
      {
        _aeabi_memset(&v32, 16, 255);
        _aeabi_memcpy(**((_DWORD **)&NandDriverInfo + 5), *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4), 2048);
        if ( !v5 )
        {
          v14 = ((v1 % *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 4) + 5) << 10) | 0x4000) & 0xFFFF | 0xAA;
          v33 = -86;
          v34 = BYTE1(v14);
        }
        v35 = 85;
        v30 = **((_DWORD **)&NandDriverInfo + 5);
        v31 = &v32;
        _R0 = *((_BYTE *)&NandStorageInfo + 10);
        _R1 = *((_BYTE *)&NandStorageInfo + 9);
        __asm { SMULBB          R0, R0, R1 }
        _R1 = *((_BYTE *)&NandStorageInfo + 10);
        _R2 = *((_BYTE *)&NandStorageInfo + 9);
        __asm { SMULBB          R1, R1, R2 }
        v29 = (1 << (_R0 - 1)) | ((1 << (_R1 - 1)) - 1);
        LML_CalculatePhyOpPar(&v28, v1, v3, v5);
        LML_VirtualPageWrite(&v28);
        if ( !PHY_SynchBank(v28, 0) )
          break;
        v27 = v3;
        if ( LML_BadBlkManage(&v27, v1, 0, &v26) )
        {
          OSAL_printf("write blk map table : bad block mange err after write\n");
          return -1;
        }
        v3 = v26;
        v5 = 0;
      }
      _aeabi_memcpy(
        **((_DWORD **)&NandDriverInfo + 5),
        *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4) + 2048,
        2048);
      v21 = v5 + 1;
      v30 = **((_DWORD **)&NandDriverInfo + 5);
      _aeabi_memset(&v32, 16, 255);
      v35 = 85;
      LML_CalculatePhyOpPar(&v28, v1, v3, v21);
      LML_VirtualPageWrite(&v28);
      if ( !PHY_SynchBank(v28, 0) )
        break;
      v27 = v3;
      if ( LML_BadBlkManage(&v27, v1, 0, &v26) )
      {
        OSAL_printf("write blk map table : bad block mange err after write\n");
        return -1;
      }
      v3 = v26;
      v5 = 0;
    }
    v22 = v21 + 1;
    _R2 = *((_BYTE *)&NandStorageInfo + 10);
    _R3 = *((_BYTE *)&NandStorageInfo + 9);
    __asm { SMULBB          R2, R2, R3 }
    _aeabi_memset(**((_DWORD **)&NandDriverInfo + 5), _R2 << 9, 255);
    _aeabi_memcpy(**((_DWORD **)&NandDriverInfo + 5), *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8), 64);
    *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 5) + 2044) = GetTblCheckSum(**((_DWORD **)&NandDriverInfo + 5), 0x10u);
    LML_CalculatePhyOpPar(&v28, v1, v3, v22);
    LML_VirtualPageWrite(&v28);
    if ( !PHY_SynchBank(v28, 0) )
      break;
    v27 = v3;
    if ( LML_BadBlkManage(&v27, v1, 0, &v26) )
    {
      OSAL_printf("write blk map table : bad block mange err after write\n");
      return -1;
    }
    v3 = v26;
    v5 = 0;
  }
  *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * v1) = v3;
  *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * v1 + 2) = v22 - 2;
  return 0;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);
// 1FC0: using guessed type int __fastcall _aeabi_memcpy(_DWORD, _DWORD, _DWORD);
// 1FCC: using guessed type int __fastcall LML_CalculatePhyOpPar(_DWORD, _DWORD, _DWORD, _DWORD);
// 1FD0: using guessed type int __fastcall LML_BadBlkManage(_DWORD, _DWORD, _DWORD, _DWORD);
// 1FD4: using guessed type int __fastcall PHY_SynchBank(_DWORD, _DWORD);
// 1FD8: using guessed type int __fastcall LML_VirtualPageWrite(_DWORD);
// 1FDC: using guessed type int __fastcall _aeabi_memset(_DWORD, _DWORD, _DWORD);
// 1FE8: using guessed type int __fastcall LML_VirtualBlkErase(_DWORD, _DWORD);

//----- (00001934) --------------------------------------------------------
signed int __fastcall read_block_map_tbl(int a1, int a2, int a3, int a4)
{
  int v4; // r4@1
  int v5; // r6@1
  int v6; // r5@1
  signed int result; // r0@2
  int v8; // r5@4
  int v9; // r3@6
  int v10; // [sp+0h] [bp-20h]@1
  int v11; // [sp+4h] [bp-1Ch]@1
  int v12; // [sp+8h] [bp-18h]@1
  int v13; // [sp+Ch] [bp-14h]@1

  v10 = a1;
  v11 = a2;
  v12 = a3;
  v13 = a4;
  v4 = a1;
  v5 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * a1);
  v6 = *(_WORD *)(*((_DWORD *)&NandDriverInfo + 1) + 4 * a1 + 2);
  v12 = **((_DWORD **)&NandDriverInfo + 5);
  v13 = 0;
  v11 = 15;
  LML_CalculatePhyOpPar(&v10, a1, v5, v6);
  if ( LML_VirtualPageRead(&v10) >= 0 )
  {
    _aeabi_memcpy(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4), **((_DWORD **)&NandDriverInfo + 5), 2048);
    v8 = v6 + 1;
    v12 = **((_DWORD **)&NandDriverInfo + 5);
    LML_CalculatePhyOpPar(&v10, v4, v5, v8);
    if ( LML_VirtualPageRead(&v10) >= 0 )
    {
      _aeabi_memcpy(
        *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4) + 2048,
        **((_DWORD **)&NandDriverInfo + 5),
        2048);
      v9 = 1023 - **((_WORD **)&NandDriverInfo + 4);
      if ( GetTblCheckSum(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4), 0x3FFu) == *(_DWORD *)(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4) + 4092) )
      {
        v12 = **((_DWORD **)&NandDriverInfo + 5);
        LML_CalculatePhyOpPar(&v10, v4, v5, v8 + 1);
        if ( LML_VirtualPageRead(&v10) >= 0 )
        {
          if ( GetTblCheckSum(**((_DWORD **)&NandDriverInfo + 5), 0x10u) == *(_DWORD *)(**((_DWORD **)&NandDriverInfo + 5)
                                                                                      + 2044) )
          {
            _aeabi_memcpy(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 8), **((_DWORD **)&NandDriverInfo + 5), 64);
            result = 0;
          }
          else
          {
            OSAL_printf("_read_block_map_tbl : read log block table checksum err\n");
            dump(**((_DWORD **)&NandDriverInfo + 5), 0x1000u, 2u, 8u);
            result = -1;
          }
        }
        else
        {
          OSAL_printf("_read_block_map_tbl : read block map table2 err\n");
          result = -1;
        }
      }
      else
      {
        OSAL_printf("_read_block_map_tbl : read data block map table checksum err\n");
        dump(*(_DWORD *)(**((_DWORD **)&NandDriverInfo + 2) + 4), 0x1000u, 4u, 8u);
        result = -1;
      }
    }
    else
    {
      OSAL_printf("_read_block_map_tbl : read block map table1 err\n");
      result = -1;
    }
  }
  else
  {
    OSAL_printf("_read_block_map_tbl :read block map table0 err\n");
    result = -1;
  }
  return result;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);
// 1FC0: using guessed type int __fastcall _aeabi_memcpy(_DWORD, _DWORD, _DWORD);
// 1FC8: using guessed type int __fastcall LML_VirtualPageRead(_DWORD);
// 1FCC: using guessed type int __fastcall LML_CalculatePhyOpPar(_DWORD, _DWORD, _DWORD, _DWORD);

//----- (00001CB0) --------------------------------------------------------
signed int __fastcall blk_map_tbl_cache_post(unsigned __int8 a1)
{
  unsigned __int8 v1; // r4@1
  unsigned __int8 v2; // r0@1
  int v3; // r2@1
  int v4; // r3@1
  _DWORD *v5; // r1@1
  signed int result; // r0@3

  v1 = a1;
  v2 = find_blk_tbl_post_location();
  v5 = (_DWORD *)*((_DWORD *)&NandDriverInfo + 2);
  *v5 = *((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * v2;
  if ( *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 2) + 1)
    && write_back_block_map_tbl(***((_BYTE ***)&NandDriverInfo + 2)) )
  {
    OSAL_printf("_blk_map_tbl_cache_post : write back zone tbl err\n");
    result = -1;
  }
  else if ( read_block_map_tbl(v1, (int)v5, v3, v4) )
  {
    OSAL_printf("_blk_map_tbl_cache_post : read zone tbl err\n");
    result = -1;
  }
  else
  {
    ***((_BYTE ***)&NandDriverInfo + 2) = v1;
    result = 0;
    *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 2) + 1) = 0;
  }
  return result;
}
// 1FBC: using guessed type int OSAL_printf(const char *, ...);

//----- (00001D6C) --------------------------------------------------------
signed int __fastcall BMM_SwitchMapTbl(int a1)
{
  unsigned __int8 v1; // r4@1
  signed int v2; // r5@1

  v1 = a1;
  v2 = 0;
  if ( blk_map_tbl_cache_hit(a1) )
    v2 = blk_map_tbl_cache_post(v1);
  CalBlkTblAccessCount();
  return v2;
}

//----- (00001DA0) --------------------------------------------------------
signed int BMM_WriteBackAllMapTbl()
{
  int v0; // r5@1
  int v1; // r6@1
  int i; // r4@1

  v0 = **((_DWORD **)&NandDriverInfo + 2);
  v1 = **((_DWORD **)&NandDriverInfo + 3);
  for ( i = 0; i < 4; i = (i + 1) & 0xFF )
  {
    if ( *(_BYTE *)(*((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i + 1) )
    {
      **((_DWORD **)&NandDriverInfo + 2) = *((_DWORD *)&NandDriverInfo + 2) + 4 + 20 * i;
      if ( write_back_block_map_tbl(***((_BYTE ***)&NandDriverInfo + 2)) )
        return -1;
      *(_BYTE *)(**((_DWORD **)&NandDriverInfo + 2) + 1) = 0;
    }
  }
  **((_DWORD **)&NandDriverInfo + 2) = v0;
  **((_DWORD **)&NandDriverInfo + 3) = v1;
  return 0;
}

// ALL OK, 28 function(s) have been successfully decompiled
