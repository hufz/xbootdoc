/* This file has been generated by the Hex-Rays decompiler.
   Copyright (c) 2007-2015 Hex-Rays <info@hex-rays.com>

   Detected compiler: GNU C++
*/

#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

_DWORD *__fastcall ebase_init_critical(_DWORD *result);
unsigned int ebase_enter_critical();
unsigned int __fastcall ebase_exit_critical(unsigned int result);
int LML_WearLevelling();


//----- (00000000) --------------------------------------------------------
_DWORD *__fastcall ebase_init_critical(_DWORD *result)
{
  *result = 0;
  return result;
}

//----- (0000000C) --------------------------------------------------------
unsigned int ebase_enter_critical()
{
  unsigned int result; // r0@1

  result = __get_CPSR();
  __set_CPSR(result | 0x80);
  return result;
}

//----- (00000024) --------------------------------------------------------
unsigned int __fastcall ebase_exit_critical(unsigned int result)
{
  __set_CPSR(result);
  return result;
}

//----- (00000034) --------------------------------------------------------
int LML_WearLevelling()
{
  return 0;
}

// ALL OK, 4 function(s) have been successfully decompiled
