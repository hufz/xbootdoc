/* This file has been generated by the Hex-Rays decompiler.
   Copyright (c) 2007-2015 Hex-Rays <info@hex-rays.com>

   Detected compiler: GNU C++
*/

#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

void OSAL_RegISR();
void OSAL_UnRegISR();
void OSAL_InterruptEnable();
void OSAL_InterruptDisable();


//----- (00000000) --------------------------------------------------------
void OSAL_RegISR()
{
  __asm { SVC             0x700 }
}

//----- (00000038) --------------------------------------------------------
void OSAL_UnRegISR()
{
  __asm { SVC             0x701 }
}

//----- (00000058) --------------------------------------------------------
void OSAL_InterruptEnable()
{
  __asm { SVC             0x706 }
}

//----- (0000006C) --------------------------------------------------------
void OSAL_InterruptDisable()
{
  __asm { SVC             0x705 }
}

// ALL OK, 4 function(s) have been successfully decompiled
