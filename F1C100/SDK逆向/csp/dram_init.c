/* This file has been generated by the Hex-Rays decompiler.
   Copyright (c) 2007-2015 Hex-Rays <info@hex-rays.com>

   Detected compiler: GNU C++
*/

#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

signed int DRAMC_initial();
int DRAMC_delay_scan();
unsigned int __fastcall DRAMC_delay_tune(unsigned int result);
unsigned int __fastcall CSP_DRAMC_set_autofresh_cycle(unsigned int result);
int __fastcall DRAMC_para_setup(int a1);
int __fastcall DRAMC_check_delay(int a1);
signed int __fastcall CSP_DRAMC_check_type(int a1);
int __fastcall CSP_DRAMC_scan_readpipe(unsigned int a1);
int CSP_DRAMC_get_dram_size();
int CSP_DRAMC_exit();
signed int __fastcall CSP_DRAMC_init(int a1, int a2);
// int __fastcall _aeabi_memcpy4(_DWORD, _DWORD, _DWORD); weak
// int __fastcall CSP_OSAL_PHY_2_VIRT(_DWORD, _DWORD); weak

//-------------------------------------------------------------------------
// Data declarations

int gDramcVirtAddr = 29364224; // weak
int gDramPioBase = 29493248; // weak
int gDramCcmBase = 29491200; // weak
_UNKNOWN dram_para; // weak
_UNKNOWN unk_92C; // weak
int dword_930; // weak
char algn_934[12]; // weak
char byte_940; // weak
char algn_941[7]; // weak
int dword_948; // weak
int dword_94C; // weak


//----- (00000000) --------------------------------------------------------
signed int DRAMC_initial()
{
  signed int result; // r0@1
  bool v1; // zf@5

  result = 0xFFFFFF;
  *(_DWORD *)(gDramcVirtAddr + 12) |= 1u;
  do
  {
    if ( !(*(_DWORD *)(gDramcVirtAddr + 12) & 1) )
      break;
    v1 = result-- == 0;
    if ( v1 )
      break;
  }
  while ( result );
  return result;
}
// 91C: using guessed type int gDramcVirtAddr;

//----- (00000054) --------------------------------------------------------
int DRAMC_delay_scan()
{
  int result; // r0@2

  *(_DWORD *)(gDramcVirtAddr + 36) = 1;
  do
    result = *(_DWORD *)(gDramcVirtAddr + 36);
  while ( result & 1 );
  return result;
}
// 91C: using guessed type int gDramcVirtAddr;

//----- (00000084) --------------------------------------------------------
unsigned int __fastcall DRAMC_delay_tune(unsigned int result)
{
  unsigned int v1; // r1@2
  int v2; // r2@2
  int v3; // r3@2

  if ( ((*(_DWORD *)(gDramcVirtAddr + 12) >> 6) & 7) == 4 )
  {
    v1 = *(_DWORD *)(gDramcVirtAddr + 36);
    v2 = (v1 >> 8) & 0x3F;
    v3 = (v1 >> 16) & 0x3F;
    if ( result < 0xC0 )
    {
      if ( result >= 0x78 )
      {
        v2 -= 8;
        v3 -= 8;
      }
    }
    else
    {
      v2 = 1;
      v3 = 1;
    }
    if ( v2 < 0 )
      v2 = 0;
    if ( v3 < 0 )
      v3 = 0;
    *(_DWORD *)(gDramcVirtAddr + 36) = (v2 << 8) | (v3 << 16);
  }
  return result;
}
// 91C: using guessed type int gDramcVirtAddr;

//----- (0000011C) --------------------------------------------------------
unsigned int __fastcall CSP_DRAMC_set_autofresh_cycle(unsigned int result)
{
  unsigned int v1; // r2@1
  unsigned int v2; // r3@1
  bool v3; // cf@2
  unsigned int v4; // r1@6
  bool v5; // cf@11
  unsigned int v6; // r1@15
  bool v7; // cf@17
  bool v8; // zf@17

  v1 = 0;
  v2 = (*(_DWORD *)gDramcVirtAddr & 0x1E0u) >> 5;
  if ( v2 == 12 )
  {
    v3 = result >= 0xF4000;
    if ( result >= 0xF4000 )
      v3 = result - 999424 >= 0x240;
    if ( v3 )
    {
      v4 = result + (result >> 3) + (result >> 4) + (result >> 5);
      while ( v4 >= 0x2625A )
      {
        v4 -= 156250;
        ++v1;
      }
    }
    else
    {
      v1 = 499 * result >> 6;
    }
  }
  else if ( v2 == 11 )
  {
    v5 = result >= 0xF4000;
    if ( result >= 0xF4000 )
      v5 = result - 999424 >= 0x240;
    if ( v5 )
    {
      v6 = result + (result >> 3) + (result >> 4) + (result >> 5);
      while ( 1 )
      {
        v7 = v6 >= 0x13000;
        v8 = v6 == 77824;
        if ( v6 >= 0x13000 )
        {
          v7 = v6 - 77824 >= 0x12C;
          v8 = v6 == 78124;
        }
        if ( v8 || !v7 )
          break;
        v6 -= 78125;
        ++v1;
      }
    }
    else
    {
      v1 = 499 * result >> 5;
    }
  }
  *(_DWORD *)(gDramcVirtAddr + 16) = v1;
  return result;
}
// 91C: using guessed type int gDramcVirtAddr;

//----- (000001EC) --------------------------------------------------------
int __fastcall DRAMC_para_setup(int a1)
{
  int v1; // r3@1
  int v2; // r1@1
  int v3; // r0@2

  v1 = a1;
  v2 = *(_DWORD *)(a1 + 20) | 2 | 8 * (*(_DWORD *)(a1 + 40) >> 2) | 16 * (*(_DWORD *)(a1 + 16) >> 1) | 32 * (*(_DWORD *)(a1 + 36) - 1) | ((*(_DWORD *)(a1 + 32) - 1) << 9);
  if ( *(_BYTE *)(a1 + 24) )
    v3 = *(_DWORD *)(a1 + 28) >> 4;
  else
    v3 = *(_DWORD *)(a1 + 28) >> 5;
  *(_DWORD *)gDramcVirtAddr = v2 | (v3 << 13) | (*(_DWORD *)(v1 + 12) << 15) | (*(_BYTE *)(v1 + 24) << 16);
  *(_DWORD *)(gDramcVirtAddr + 12) |= 0x80000u;
  DRAMC_initial();
  return 0;
}
// 91C: using guessed type int gDramcVirtAddr;

//----- (00000298) --------------------------------------------------------
int __fastcall DRAMC_check_delay(int a1)
{
  int v1; // r1@1
  int result; // r0@1
  unsigned int v3; // r2@1
  unsigned int v4; // r4@2
  unsigned int i; // r3@4
  unsigned int j; // r12@13

  v1 = a1;
  result = 0;
  v3 = 0;
  if ( v1 == 16 )
    v4 = 4;
  else
    v4 = 2;
  for ( i = 0; i < v4; ++i )
  {
    if ( i )
    {
      switch ( i )
      {
        case 1u:
          v3 = *(_DWORD *)(gDramcVirtAddr + 52);
          break;
        case 2u:
          v3 = *(_DWORD *)(gDramcVirtAddr + 56);
          break;
        case 3u:
          v3 = *(_DWORD *)(gDramcVirtAddr + 60);
          break;
      }
    }
    else
    {
      v3 = *(_DWORD *)(gDramcVirtAddr + 48);
    }
    for ( j = 0; j < 0x20; ++j )
    {
      if ( v3 & 1 )
        ++result;
      v3 >>= 1;
    }
  }
  return result;
}
// 91C: using guessed type int gDramcVirtAddr;

//----- (0000038C) --------------------------------------------------------
signed int __fastcall CSP_DRAMC_check_type(int a1)
{
  int v1; // r2@1
  int v2; // r5@1
  unsigned int i; // r4@1
  signed int result; // r0@7

  v1 = a1;
  v2 = 0;
  for ( i = 0; i < 8; ++i )
  {
    *(_DWORD *)(gDramcVirtAddr + 12) = *(_DWORD *)(gDramcVirtAddr + 12) & 0xFFFFFE3F | (i << 6);
    DRAMC_delay_scan();
    if ( *(_DWORD *)(gDramcVirtAddr + 36) & 0x30 )
      ++v2;
  }
  if ( v2 == 8 )
  {
    result = 0;
    *(_BYTE *)(v1 + 24) = 0;
  }
  else
  {
    result = 1;
    *(_BYTE *)(v1 + 24) = 1;
  }
  return result;
}
// 91C: using guessed type int gDramcVirtAddr;

//----- (00000410) --------------------------------------------------------
int __fastcall CSP_DRAMC_scan_readpipe(unsigned int a1)
{
  unsigned int v1; // r4@1
  unsigned int v2; // r8@1
  unsigned int v3; // r7@1
  unsigned int i; // r6@2
  signed int v5; // r7@12
  int v7; // [sp+0h] [bp-3Ch]@3

  v1 = a1;
  v2 = 0;
  v3 = 0;
  if ( byte_940 == 1 )
  {
    for ( i = 0; i < 5; ++i )
    {
      *(_DWORD *)(gDramcVirtAddr + 12) = *(_DWORD *)(gDramcVirtAddr + 12) & 0xFFFFFE3F | (i << 6);
      DRAMC_initial();
      DRAMC_delay_scan();
      *(&v7 + i) = 0;
      if ( !((*(_DWORD *)(gDramcVirtAddr + 36) >> 4) & 3) && !((*(_DWORD *)(gDramcVirtAddr + 36) >> 4) & 1) )
        *(&v7 + i) = DRAMC_check_delay(*(int *)&algn_941[3]);
      if ( *(&v7 + i) > v2 )
      {
        v2 = *(&v7 + i);
        v3 = i;
      }
    }
    *(_DWORD *)(gDramcVirtAddr + 12) = *(_DWORD *)(gDramcVirtAddr + 12) & 0xFFFFFE3F | (v3 << 6);
    DRAMC_delay_scan();
    DRAMC_delay_tune(v1);
  }
  else
  {
    *(_DWORD *)gDramcVirtAddr &= 0xFFFE9FFF;
    DRAMC_initial();
    if ( v1 > 0x5A )
    {
      if ( v1 > 0x96 )
        v5 = 3;
      else
        v5 = 2;
    }
    else
    {
      v5 = 1;
    }
    *(_DWORD *)(gDramcVirtAddr + 12) = *(_DWORD *)(gDramcVirtAddr + 12) & 0xFFFFFE3F | (v5 << 6);
  }
  return 0;
}
// 91C: using guessed type int gDramcVirtAddr;
// 940: using guessed type char byte_940;

//----- (00000580) --------------------------------------------------------
int CSP_DRAMC_get_dram_size()
{
  unsigned int v0; // r4@1
  int v1; // r6@1
  unsigned int i; // r4@4
  signed int v3; // r7@10
  int v4; // r6@12
  signed int v5; // r10@13
  signed int v6; // r11@13
  unsigned int j; // r4@15
  unsigned int k; // r4@18
  signed int v9; // r8@24

  v0 = 0;
  v1 = 0;
  dword_948 = 10;
  dword_94C = 13;
  DRAMC_para_setup((int)&dram_para);
  CSP_DRAMC_scan_readpipe(dword_930);
  while ( v0 < 0x20 )
  {
    *(_DWORD *)(v0 - 2147483136) = 286331153;
    *(_DWORD *)(v0++ - 2147482112) = 572662306;
  }
  for ( i = 0; i < 0x20; ++i )
  {
    if ( *(_DWORD *)(i - 2147483136) == 572662306 )
      ++v1;
  }
  if ( v1 == 32 )
    v3 = 9;
  else
    v3 = 10;
  v4 = 0;
  dword_948 = v3;
  dword_94C = 13;
  DRAMC_para_setup((int)&dram_para);
  if ( v3 == 10 )
  {
    v5 = -2143289344;
    v6 = -2134900736;
  }
  else
  {
    v5 = -2145386496;
    v6 = -2141192192;
  }
  for ( j = 0; j < 0x20; ++j )
  {
    *(_DWORD *)(v5 + j) = 858993459;
    *(_DWORD *)(v6 + j) = 1145324612;
  }
  for ( k = 0; k < 0x20; ++k )
  {
    if ( *(_DWORD *)(v5 + k) == 1145324612 )
      ++v4;
  }
  if ( v4 == 32 )
    v9 = 12;
  else
    v9 = 13;
  dword_948 = v3;
  dword_94C = v9;
  if ( v9 == 13 )
  {
    if ( dword_948 == 10 )
      unk_92C = 64;
    else
      unk_92C = 32;
  }
  else
  {
    unk_92C = 16;
  }
  CSP_DRAMC_set_autofresh_cycle(dword_930);
  *(_DWORD *)algn_934 = 0;
  DRAMC_para_setup((int)&dram_para);
  return 0;
}
// 930: using guessed type int dword_930;
// 948: using guessed type int dword_948;
// 94C: using guessed type int dword_94C;

//----- (00000710) --------------------------------------------------------
int CSP_DRAMC_exit()
{
  return 0;
}

//----- (0000071C) --------------------------------------------------------
signed int __fastcall CSP_DRAMC_init(int a1, int a2)
{
  int v2; // r4@1
  int v3; // r5@1
  signed int result; // r0@2
  int v5; // r6@7
  unsigned int v6; // r6@8
  int v7; // r6@10
  unsigned int v8; // r6@11

  v2 = a1;
  v3 = a2;
  gDramcVirtAddr = CSP_OSAL_PHY_2_VIRT(29364224, 1024);
  gDramPioBase = CSP_OSAL_PHY_2_VIRT(29493248, 1024);
  gDramCcmBase = CSP_OSAL_PHY_2_VIRT(29491200, 1024);
  if ( v2 )
  {
    if ( v3 )
    {
      while ( *(_DWORD *)(gDramcVirtAddr + 12) & 1 )
        ;
      *(_DWORD *)v2 = 2147483648;
      *(_DWORD *)(v2 + 4) = 64;
      *(_DWORD *)(v2 + 12) = 1;
      *(_DWORD *)(v2 + 16) = 1;
      *(_DWORD *)(v2 + 20) = 0;
      *(_BYTE *)(v2 + 24) = 1;
      *(_DWORD *)(v2 + 28) = 16;
      *(_DWORD *)(v2 + 32) = 10;
      *(_DWORD *)(v2 + 36) = 13;
      *(_DWORD *)(v2 + 40) = 4;
      *(_DWORD *)(v2 + 44) = 3;
      _aeabi_memcpy4(&dram_para, v2, 48);
      v5 = *(_DWORD *)(gDramPioBase + 548);
      if ( byte_940 == 1 )
        v6 = v5 | 0x10000;
      else
        v6 = v5 & 0xFFFEFFFF;
      *(_DWORD *)(gDramPioBase + 548) = v6;
      *(_DWORD *)(gDramcVirtAddr + 4) = -1479620294;
      *(_DWORD *)(gDramcVirtAddr + 8) = 5701640;
      *(_DWORD *)(gDramcVirtAddr + 20) = 0x10000;
      DRAMC_para_setup((int)&dram_para);
      CSP_DRAMC_check_type((int)&dram_para);
      v7 = *(_DWORD *)(gDramPioBase + 548);
      if ( byte_940 == 1 )
        v8 = v7 | 0x10000;
      else
        v8 = v7 & 0xFFFEFFFF;
      *(_DWORD *)(gDramPioBase + 548) = v8;
      CSP_DRAMC_get_dram_size();
      result = 0;
    }
    else
    {
      _aeabi_memcpy4(&dram_para, v2, 48);
      result = 0;
    }
  }
  else
  {
    result = 1;
  }
  return result;
}
// 91C: using guessed type int gDramcVirtAddr;
// 920: using guessed type int gDramPioBase;
// 924: using guessed type int gDramCcmBase;
// 940: using guessed type char byte_940;
// 95C: using guessed type int __fastcall _aeabi_memcpy4(_DWORD, _DWORD, _DWORD);
// 960: using guessed type int __fastcall CSP_OSAL_PHY_2_VIRT(_DWORD, _DWORD);

// ALL OK, 11 function(s) have been successfully decompiled
